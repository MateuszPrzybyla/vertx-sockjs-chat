import {Injectable} from "@angular/core";
import * as EventBus from "vertx3-eventbus-client";
import {Observable} from "rxjs/Observable";
import {ReplaySubject} from "rxjs/ReplaySubject";

@Injectable()
export class WebSocketService {

  private eventBusSocket: EventBus;

  public connect(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.eventBusSocket = new EventBus('http://localhost:8080/ws');
      this.eventBusSocket.onopen = function () {
        console.log('WebSocket connection opened');
        resolve();
      };
    });
  }

  public send(address, msg: any): void {
    this.eventBusSocket.publish(address, msg);
  }

  public registerHandler(address: string): Observable<any> {
    let subject = new ReplaySubject();
    this.eventBusSocket.registerHandler(address, (error, message) => {
      if (error) {
        subject.error(error);
      } else {
        subject.next(message.body);
      }
    });
    return subject;
  }

}
