import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {ChatComponent} from "./chat.component";
import {WebSocketService} from "./websocket.service";


@NgModule({
  declarations: [
    AppComponent, ChatComponent
  ],
  imports: [
    BrowserModule, FormsModule
  ],
  providers: [WebSocketService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
