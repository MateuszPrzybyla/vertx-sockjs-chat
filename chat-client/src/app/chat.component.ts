import {Component, OnInit} from '@angular/core';
import {WebSocketService} from "./websocket.service";

@Component({
  selector: 'chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  messages: string[] = [];
  inputValue: string = "";

  constructor(private webSocketService: WebSocketService) {
  }

  ngOnInit(): void {
    this.webSocketService.connect().then(() => {
      this.webSocketService.registerHandler("chat.123").subscribe(msg => {
        this.messages.push(msg);
      });
    });
  }

  send(): void {
    if (this.inputValue.length != 0) {
      this.webSocketService.send("chat.123", this.inputValue);
      this.inputValue = "";
    }
  }
}
