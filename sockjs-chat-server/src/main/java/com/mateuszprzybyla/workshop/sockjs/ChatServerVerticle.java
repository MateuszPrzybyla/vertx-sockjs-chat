package com.mateuszprzybyla.workshop.sockjs;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;

import java.util.concurrent.ThreadLocalRandom;

public class ChatServerVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChatServerVerticle.class);

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        Router router = Router.router(vertx);

        PermittedOptions permitChatOptions = new PermittedOptions().setAddressRegex("chat\\..*");
        BridgeOptions options = new BridgeOptions()
                .addInboundPermitted(permitChatOptions)
                .addOutboundPermitted(permitChatOptions);
        SockJSHandler sockJSHandler = SockJSHandler.create(vertx).bridge(options, event -> {
            LOGGER.info("SockJS event, type {0}, message: {1}", event.type(), event.getRawMessage());
            event.complete(true);
        });
        vertx.setPeriodic(1000, timerId -> {
            vertx.eventBus().publish("chat.123", "server " + ThreadLocalRandom.current().nextInt());
        });

        router.route("/ws/*").handler(sockJSHandler);

        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(8080, result -> {
                    if (result.succeeded()) {
                        LOGGER.info("HTTP server is now listening on port 8080...");
                        startFuture.complete();
                    } else {
                        LOGGER.error("HTTP failed to start on port 8080", result.cause());
                        startFuture.fail(result.cause());
                    }
                });
    }

}
